import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class CalculatorTest {
    @Test
    public void whenAddOnePlusOneThenTwo()  {
        Calculator calc = new Calculator();
        int result = calc.calculate(1, 1, '+');
        int expected = 2;
        assertThat(result, is(expected));
    }
    @Test
    public void whenMinusOnePlusOneThenZero()  {
        Calculator calc = new Calculator();
        int result = calc.calculate(1, 1, '-');
        int expected = 0;
        assertThat(result, is(expected));
    }
    @Test
    public void whenMultiplyOneByFiveThenFive()  {
        Calculator calc = new Calculator();
        int result = calc.calculate(1, 5, '*');
        int expected = 5;
        assertThat(result, is(expected));
    }
    @Test
    public void whenDivideTenByFiveThenTwo()  {
        Calculator calc = new Calculator();
        int result = calc.calculate(10, 5, '/');
        int expected = 2;
        assertThat(result, is(expected));
    }
}
