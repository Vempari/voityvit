import java.util.Scanner;

public class IfWhile {
	public static void main(String[] args) {
		// Здесь был коммент
		/*
			asklfjsalk;djf
			sdflkjsdlkfj
		 */
		/**
		 * if (вариант1)
		 * {… }
		 * else if (вариант2)
		 * {…}
		 * …
		 * else if (вариантN) ….
		 * else {
		 *
		 * }
		 *
		 *
		 * switch (ВыражениеДляВыбора) {
		 *            case  (Значение1):
		 *                Код1;
		 *                break;
		 *            case (Значение2):
		 *                Код2;
		 *                break;
		 * ...
		 *            case (ЗначениеN):
		 *                КодN;
		 *                break;
		 *            default:
		 *                КодВыбораПоУмолчанию;
		 *                break;
		 *        }
		 */

		int a = 0;
		int b = 10;
		int c = 1;
		//  & - логическое и
		// | - логическое или
//		!= не равно
//		== - равно
//		^ - исключающая или XOR
		// > - больше
		// < - меньше
		// >= - больше либо равно
		// <= - меньше либо равно
		if (a > b & a > c) {
			System.out.println("a > b и a > c");
		} else if (b > a & b > c) {
			System.out.println("b > a && b > c");
		} else {
			System.out.println("c больше всех!");
		}




		Scanner sc = new Scanner(System.in);


		switch (sc.nextLine()) {
			case ("Пока"):
				System.out.println("пока!");
				break;
			case ("Привет"):
				System.out.println("Привет!");
				break;
			default:
				System.out.println("Всего доброго");
		}

	}


}
